/************************************************************************************/
/* FILE		      : adc.c   									                                   	    */
/* AUTHOR		    : you																																*/
/*----------------------------------------------------------------------------------*/
/* COMMENTS     : Offers ADC function ued on PIC18F6680	and PICEBS board     				*/
/*----------------------------------------------------------------------------------*/
/* REVISION     : 1.0 (02-2013) - tested with HI-TECH compiler                      */
/************************************************************************************/
#include <htc.h>
#include <stdint.h>
#include "adc.h"
#define MEASNUM 64

/************************************************************************************/
/* DETAILS      : see .h definition file                                      	    */
/************************************************************************************/
void ADC_init(void)
{
    //I/O config
	TRISA = 0xFF;// Config PORT A as input  
    
    //ADCON1 
    ADCON1bits.VCFG0 = 0;
    ADCON1bits.VCFG1 = 0; // VRef+ = AVDD
    ADCON1bits.VNCFG = 0; // VREF - = AVss
    
    //ADCON2
    ADCON2bits.ADCS = 1; // fosc/8
    ADCON2bits.ACQT = 2; // 4 TAD
    ADFM = 1; // ADRES align set to right
    
}

/************************************************************************************/
/* DETAILS      : see .h definition file                                      	    */
/************************************************************************************/
uint16_t ADC_measure(uint8_t channel)
{
	uint32_t sum = 0;
    uint16_t retval = 0;
    ADCON0bits.CHS = channel ; // set analog channel
    ADON = 1;// Convert enable
    
    for(int i = 0; i < MEASNUM ; i++)
    {
        ADCON0bits.GODONE =1;// BEGINS CONVERTING
        while(ADCON0bits.GODONE)// IS CONVERTING
        {};
        sum += ADRES;
    }
    retval = (uint16_t)(sum/MEASNUM);
    return retval;
}

