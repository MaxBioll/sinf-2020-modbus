/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */
#include <stdio.h>
#endif

#include "lcd_highlevel.h"          /* User funct/params, such as InitApp */
#include "lcd_bigfont.h"
#include "tsc.h"
#include "uart.h"
#include "adc.h"
#include "modbus.h"
#include "eeprom.h"
#include "pwm.h"

// Definition of ADC channels
#define VOLTAGE_CHANNEL 2
#define CURRENT_CHANNEL 1

 // Gain défini par les res. pour la mesure du courant
#define VOLTTOCURRENT 200
// Lcd font 
extern const FONT_INFO arialNarrow_12ptFontInfo;

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

void main(void)
{
    // INITS
    ADC_init();
    MODBUS_init();
    PWM_init();
    UART_init();
    LCD_Init();
	
    uint16_t dutyCycle = 0; 
    uint8_t tempString[40];
	
	//Variables for measures
    uint16_t offset;
    uint16_t temp16;
    uint32_t temp_voltage, temp_current;
    
    // Set duty cycle to 0 to get current OFFSET
    PWM_set(0); 
    __delay_ms(2);
    offset = ADC_measure(CURRENT_CHANNEL);
    
    // LCD screen config
    LCD_Fill(WHITE);
    //Show current
    sprintf(tempString,"Voltage value = ");
    LCD_DrawText(tempString, arialNarrow_12ptFontInfo,A_CENTER,100,30, BLACK, WHITE);
    sprintf(tempString," [mV] ");
    //Show voltage
    LCD_DrawText(tempString, arialNarrow_12ptFontInfo,A_CENTER,220,30, BLACK, WHITE);
    sprintf(tempString,"Current value = ");
    LCD_DrawText(tempString,arialNarrow_12ptFontInfo,A_CENTER,100,60, BLACK, WHITE);
    sprintf(tempString," [uA]");
    LCD_DrawText(tempString, arialNarrow_12ptFontInfo,A_CENTER,220,60, BLACK, WHITE);
    // Show pwm
    sprintf(tempString,"Duty Cycle = ");
    LCD_DrawText(tempString,arialNarrow_12ptFontInfo,A_CENTER,100,90, BLACK, WHITE);
    sprintf(tempString," %% ");
    LCD_DrawText(tempString,arialNarrow_12ptFontInfo,A_CENTER,200,90, BLACK, WHITE);
	
	// Forever loop for measures
	for(;;)
	{     
        temp_current = 0;
        temp_voltage = 0;
                              
        //Measure of voltage 
        temp_voltage = ADC_measure(VOLTAGE_CHANNEL);
        temp_voltage = (temp_voltage * 3300)/ 4096;             
        inputRegisters[0] = (uint16_t)temp_voltage;
        
        // Measure of current
        temp16 = (int32_t)(ADC_measure(CURRENT_CHANNEL));      
        if(temp16<offset)
        {
            temp16 = 0; // Case where the offset gets a bigger 
        }
        temp_current = (uint32_t)temp16-offset;
        temp_current = (temp_current * 3300)/4096;
        temp_current = (temp_current*1000)/VOLTTOCURRENT;        
        inputRegisters[1] = (uint16_t)temp_current;
		
		// PWM SET TO NEW VALUE                  
        PWM_set(holdingRegisters[0]);
        
        // LCD show live values
		// Voltage value
        sprintf(tempString," %d ", temp_voltage);    
        LCD_DrawText(tempString, arialNarrow_12ptFontInfo,A_CENTER,180,30, BLACK, WHITE);
		// Current value
        sprintf(tempString," %d ", temp_current);
        LCD_DrawText(tempString,arialNarrow_12ptFontInfo,A_CENTER,180,60, BLACK, WHITE);
        // PWM duty cycle
        dutyCycle = (holdingRegisters[0]>>2)*100/PR2; 
        sprintf(tempString," %d ", dutyCycle);
        LCD_DrawText(tempString,arialNarrow_12ptFontInfo,A_CENTER,170,90, BLACK, WHITE);
 

	}
}


