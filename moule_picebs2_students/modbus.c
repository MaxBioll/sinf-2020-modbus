/************************************************************************************/
/* FILE		      : modbus.c 									                                   	    */
/* AUTHOR		    : you																																*/
/*----------------------------------------------------------------------------------*/
/* COMMENTS     : Offers MODBUS functions ued on PIC18F6680	and PICEBS board     		*/
/*----------------------------------------------------------------------------------*/
/* REVISION     : 1.0 (02-2013) - tested with HI-TECH compiler                      */
/************************************************************************************/
#include <htc.h>
#include <stdint.h>
#include "uart.h"
#include "crc.h"
#include "pwm.h"


uint16_t inputRegisters[10];		// MODBUS table of registers
uint16_t holdingRegisters[10];		// MODBUS table of registers
static uint8_t addrEepromPos = 0;	// TODO, Load PIC address in eeprom


/************************************************************************************/
/* DETAILS      : see .h definition file                                      	    */
/************************************************************************************/
void MODBUS_init(void)
{
    holdingRegisters[1] = 0x80;
    inputRegisters[0] = 0;
    inputRegisters[1] = 0;
    
    // Init Timer to count to 3.5 chars 
    T0CS = 0; // FOSC/4
    PSA = 0 ; // NEED PRESCALER
    T0CONbits.T0PS = 0; // Prescaler set to 2 -> 1 MHZ -> 1us
    T0CONbits.T08BIT = 0; // set to 16 bits tmr0
    TMR0ON = 0;
    TMR0 = 61400; // Counts to more than 3.5 chars
    TMR0IE = 1; // en 
    TMR0IF = 0;
}

/************************************************************************************/
/* DETAILS      : see .h definition file                                      	    */
/************************************************************************************/
void MODBUS_send(  int param )
{
	
    for(int i = 0; i < param;i++)
    {        
         UART_write(txBuffer[i]);
    }
}

void send_error_msg(uint8_t errorNum, uint8_t eCode)
{
   txBuffer[1] = 80 + errorNum;//
   txBuffer[2] = eCode;// EXCEPTION CODE
}
/************************************************************************************/
/* DETAILS      : see .h definition file                                      	    */
/************************************************************************************/
uint8_t MODBUS_receiveAndAnswer(void)
{
    
    // Clear txBuffer Before any operation
    for(int i = 0; i < (int)(sizeof(txBuffer)/sizeof(txBuffer[0])) ; i++)
    {
        txBuffer[i] = 0;
    }
    
    if(rxBuffer[0]==holdingRegisters[1])// IT's our channel
    {
        // 
        txBuffer[0] = rxBuffer[0];
        uint16_t crc = (rxBuffer[bufferPos-1]<<8)+ rxBuffer[bufferPos-2]; // get CRC
        
        if(CRC16(rxBuffer,bufferPos-2) == crc )
        {
            int responseBufferSize;
            switch(rxBuffer[1])

            {
                // ======== WRITE HOLDING REGISTER ========
                case 6 : 
                {
                    // Copy of the first bytes since they are the same
                    for(int i = 0; i< bufferPos-2;i++)
                    {
                         txBuffer[i+1] = rxBuffer[i+1];
                    }
                    // write value at the right place
                    holdingRegisters[rxBuffer[3]] = ((rxBuffer[4] << 8) + rxBuffer[5]);
                                      
                    //Set response Buffer
                    responseBufferSize = 8;
                    
                    // CRC CONFIG
                    txBuffer[6]= CRC16(txBuffer,responseBufferSize-2);// SEND LSB
                    txBuffer[7]= CRC16(txBuffer,responseBufferSize-2)>>8;// THEN MSB
                }
                break;
                
                // ======== READ HOLDING REGISTERS ========
                case 3 :
                {
                    // Function code                  
                    txBuffer[1] = rxBuffer[1];
                    
                    //Check for register Address And Quantity of Address exceptions
                    ///
                    ///     TODO
                    ///                   
                    // getting only LSB of reg Address, we wont go fruther than 10 => no need MSB 
                    uint8_t registerAddress = rxBuffer[3]; 
                    
                    // Getting number of registers to return (Goes from 0 to 125 => no need MSB)
                    uint8_t registerNum = rxBuffer[5];                   
                    txBuffer[2] = registerNum *2;
                    
                    for(int i = 0 ; i <  registerNum;i++)
                    {
                        txBuffer[3+i*2] = (uint8_t)(holdingRegisters[registerAddress +i]>>8);
                        txBuffer[4+i*2] = holdingRegisters[registerAddress +i];
                    }
                    
                    // Set response buffer size
                    responseBufferSize = 3 + 2*registerNum+2;
                    
                    // CONFIG CRC
                    txBuffer[responseBufferSize-2]= CRC16(txBuffer,responseBufferSize-2);// SEND LSB
                    txBuffer[responseBufferSize-1]= CRC16(txBuffer,responseBufferSize-2)>>8;// THEN MSB
                    
                }
                break;
                
                // ======== READ INPUT REGISTERS ========
                case 4 : 
                {
                     //Header and function code
                    txBuffer[0] = rxBuffer[0];
                    txBuffer[1] = rxBuffer[1];
                    
                    //Check for register Address And Quantity of Address exceptions
                    ///
                    ///     TODO
                    ///                   
                    // getting only LSB of reg Address, we wont go fruther than 10 => no need MSB 
                    uint8_t registerAddress = rxBuffer[3]; 
                                     
                    // Getting number of registers to return (Goes from 0 to 125 => no need MSB)
                    uint8_t registerNum = rxBuffer[5];                   
                    txBuffer[2] = registerNum *2;
                    
                    for(int i = 0; i < registerNum;i++)
                    {
                        txBuffer[3+i*2] = (uint8_t)(inputRegisters[registerAddress+i]>>8);
                        txBuffer[4+i*2] = inputRegisters[registerAddress+i];
                    }
                    
                    // Set response buffer size
                    responseBufferSize = 3 + 2*registerNum+2;
                    
                    // CONFIG CRC
                    txBuffer[responseBufferSize-2]= CRC16(txBuffer,responseBufferSize-2);// SEND LSB
                    txBuffer[responseBufferSize-1]= CRC16(txBuffer,responseBufferSize-2)>>8;// THEN MSB
                    
                }
                break;
                
                // ======== WRONG FUNCTION CODE ========
                default: 
                {                   
                    responseBufferSize = 0;
                    // Exception CODE 01
                    send_error_msg(rxBuffer[1],1);
                    
                }               
            }
            
            // Send txBuffer 
            MODBUS_send(responseBufferSize);
        }
              
    }
    
    // CLEAR RX BUFFER AND RE-INIT bufferPos 
    for(int i = 0; i < bufferPos ; i++)
    {
        rxBuffer[i] = 0;
    }
    bufferPos = 0;
    
    
    return 0;
}


