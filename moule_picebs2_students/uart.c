/************************************************************************************/
/* FILE		      : uart.c   									                                   	    */
/* AUTHOR		    : you																																*/
/*----------------------------------------------------------------------------------*/
/* COMMENTS     : Offers UART function ued on PIC18F6680	and PICEBS board   				*/
/*----------------------------------------------------------------------------------*/
/* REVISION     : 1.0 (02-2013) - tested with HI-TECH compiler                      */
/************************************************************************************/
#include <htc.h>
#include <stdint.h>
#include "pwm.h"

uint8_t	rxBuffer[40];
uint8_t	txBuffer[40];
int bufferPos;
/************************************************************************************/
/* DETAILS      : see .h definition file                                      	    */
/************************************************************************************/
void UART_init(void)
{	
	// init buffer to know how long rxBuffer is
    bufferPos = 0;
    
    //UART init
    TXEN1 = 1; //Enable transmit
    TX9 = 1;// enable 9th bit
    TX9D1 = 1;//set 9th stop bit
    
    // General config
    SPEN1 = 1; //Recption enable
    CREN1 = 1;
    
    // Config chip to work. HAS TO BE CONFIGURED FOR UART
    TRISG0 = 0;
    LATG0 = 1;
    
    //Set baudrate to 9615
    SYNC1 = 0;
    BRGH1 = 0; 
    BRG161 = 0;
    SPBRG1 = 12;
    
    //Receive interrupt enable
    RC1IE = 1;
    
    //Enable interrupts for TMR0 and UART
    GIE = 1;
    PEIE = 1;  
}

/************************************************************************************/
/* DETAILS      : see .h definition file                                      	    */
/************************************************************************************/
void UART_write(uint8_t data)
{
	
    while(TX1IF == 0)
    {
        //Wait flag set
    }
    //Send data
    TXREG = data;
}

/************************************************************************************/
/* DETAILS      : see .h definition file                                      	    */
/************************************************************************************/
uint8_t UART_read(void)
{   
    // Get message and put it in rxBuffer
    rxBuffer[bufferPos] = RCREG1;
    bufferPos++;
    
    return 0;
}

